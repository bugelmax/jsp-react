import React, { useEffect, useState } from 'react';
import { Link, Route, useParams, useRouteMatch } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Users (){
    const [users, setUsers] = useState([]);

    useEffect( () => {
        async function fetchData() {
            const res = await fetch(
                `https://jsonplaceholder.typicode.com/users/`
            );
            const json = await res.json();
            setUsers(json);
        }
        fetchData();
    }, []);

    const delUser = (id) => {
        setUsers( [ ...users.filter( (user) => user.id !== id) ] )
    }
    const { url, path } = useRouteMatch();

    const User = () => {
        const { name } = useParams();

        return (
            <div className="card">
                <div className="card-name">
                    {name}
                </div>
            </div>
        )
    }
    return (
        <ul className="user-list">
            {
            users.map(
                (user) => 
                    <li key={user.id}>
                        <Link to={`${url}/${user.name}`}>open user</Link>
                        {user.name}
                        <span className="user-delete" 
                            onClick = {() => {delUser(user.id)}}
                        >
                            <FontAwesomeIcon icon={ [ "fas", "times" ] } />
                        </span>
                    </li>
                )
            }
            <Route path={`${path}/:name`}>
                <User />
            </Route>
        </ul>
    )
}
