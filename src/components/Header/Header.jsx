import React from 'react';
import { Link } from "react-router-dom";
import styles from './Header.module.css';

export default function Header () {
  return (
    <header>
      <nav className={styles.mainNavigation}>
        <Link to="/">Home</Link>
        <Link to="/users">Users</Link>
        <Link to="/todos">Todos</Link>
      </nav>
    </header>
  )
}