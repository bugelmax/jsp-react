import React, { useEffect } from 'react';
import { useState } from 'react';
import styles from './Todos.module.css';
import ToDoForm from './ToDoForm';
import Todo from './Todo';

export default function Todos(){
	const [todos, setTodos] = useState([]);
	useEffect( () => {
		async function fetchData() {
			const res = await fetch(
				`https://jsonplaceholder.typicode.com/todos/`
			);

			const json = await res.json();
			setTodos(json);
		}

		fetchData();
	}, []);

	const addTask = (taskInput) => {
		if (taskInput) {
			const newItem = {
				id: Math.random().toString(36).substr(2,9),
				title: taskInput,
				completed: false
			}
			setTodos( [...todos, newItem] )
		}
	};

	const delTask = (id) => {
		setTodos( [ ...todos.filter( (todo) => todo.id !== id) ] )
	}

	const handleToggle = (id) => {
		setTodos([
			...todos.map((todo) =>
				todo.id === id ? { ...todo, completed: !todo.completed } : { ...todo }
			)
		])
	};
	return (
		<div>
			<h1 clasName={styles.title}>Task list: { todos.length }</h1>

			<ToDoForm addTask={addTask} />

			<ul className={styles.todoList}>
      	{
          todos.map((todo, id) => <Todo
            key         = { todo.id }
            todo        = { todo }
            delTask     = { delTask }
            toggleTask  = { handleToggle }
          /> )
        }
			</ul>
		</div>
	);
}
