import React from 'react';
import styles from './Todos.module.css';
import { useState } from 'react';

function ToDoForm ({ addTask }) {
  const [ taskInput, setTaskInput ] = useState('') 

  const handleChange = (e) => {
    setTaskInput(e.currentTarget.value)
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    addTask(taskInput);
    setTaskInput('')
  };

  const handleKeyDown = (e) => {
    if ( e.key === "Enter") {
      handleSubmit(e)
    }
  };

  return (
    <form onSubmit={ handleSubmit } className={styles.form}>
      <input
        type        = "text"
        value       = { taskInput }
        onChange    = { handleChange }
        onKeyDown   = { handleKeyDown }
        placeholder = "Type new task"
      />
      <button>Save</button>
    </form>
  );
}

export default ToDoForm;
