import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Todos.module.css';
export default function Todo(
  { 
    todo,
    delTask, 
    toggleTask
  }
  ){
  return (
    <li
      className={todo.completed ? `${styles.todoItem} ${styles.check}` : `${styles.todoItem}`}
      key= { todo.id } 
    >
      { todo.title }
      <div className={styles.controls}>
        <span className="item-delete" 
          onClick = {() => {delTask(todo.id)}}
        >
          <FontAwesomeIcon icon={ [ "fas", "times" ] } />
        </span>
        <span
          onClick= {() => {toggleTask(todo.id)}} 
        >
          <FontAwesomeIcon icon={ [ "fas", "check" ] } />
        </span>
      </div>
    </li>
  );
}