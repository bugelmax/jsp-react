import React from 'react';
import styles from './App.module.css';
import Header from './components/Header/Header';
import Todos from './components/Todos/Todos';
import Home from './components/Home';
import Users from './components/Users'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
library.add( fab, faTimes, faCheck );

function App() {
  return (
    <div className={styles.App}>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/todos">
            <Todos />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="*">
            404 not found
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
export default App;
